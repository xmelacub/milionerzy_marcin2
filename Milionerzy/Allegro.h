#pragma once
#ifndef Allegro_h
#define Allegro_h

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include<allegro5/allegro_image.h>
using namespace std;
class Graj
{
public:
	bool kolo1 = true;
	bool kolo2 = true;
	bool kolo3 = true;
	int numer_pytania = 0;
	int ilosc_odpowiedzi = 4;
	string niepoprawne[3];
	string tab[10];
	string odpowiedzi[4];
	string publicznosc[10];
	string odpowiedzi2[4];
	string szansa_poprawna;
	string szansa_bledna1;
	string szansa_bledna2;
	string szansa_bledna3;
	string wylosowana;
	string wylosowana1;
	string tresc_pytania;
	string poprawna_odpowiedz;
	string wybrana_odpowiedz;
	string wybor_przyjaciela;
	string poprawna_odpowiedz2;
	void Pobierz_Odpowiedzi();
	void TablicaNiepoprawne();
	void Telefon();
	void PnP();
	void Publicznosc();
};//Klasa zawierające wyliczone przez funkcje elementy potrzebne do działania klasy Allegro.
class Allegro :public Graj
{
public:
	ALLEGRO_DISPLAY* display;
	ALLEGRO_EVENT_QUEUE* queue;
	ALLEGRO_BITMAP* backgrund;
	ALLEGRO_FONT* font;
	ALLEGRO_BITMAP* telefon;
	ALLEGRO_BITMAP* pnp;
	ALLEGRO_BITMAP* publicznosc;
	ALLEGRO_BITMAP* dobrze;
	ALLEGRO_BITMAP* brawo;
	string kasa[12] = { "500 zl","1000 zl","2000 zl","5000 zl","10000 zl","20000 zl","40000 zl","75000 zl","125000 zl","250000 zl","500000 zl","1000000 zl" };
	bool lose;
	bool good;
	bool running;
	bool info;
	bool play;
	bool win;
	float display_height;
	float display_width;
	float backgrund_height;
	float backgrund_width;
	float brawo_width;
	float brawo_height;
	float kolo_width;
	float kolo_height;
	float font_height;
	float font_GRAJ_width;
	float font_OK_width;
	float font_INSTRUKCJA_width;
	float font_WYJDZ_width;
	float font_MENU_width;
	float font_OdpA_width;
	float font_OdpB_width;
	float font_OdpC_width;
	float font_OdpD_width;
	void Start();
	void MainFunc();
	void Info();
	void Play();
	void Click();
	void Lost();
	void Good();
	void Win();
	void Szukaj_Wylosowanych();
	void Pokaz_Procenty();
	void Odpowiedz_Przyjaciela();
};//Odpowiedzialna za wyświetlanie owoców działania funkcji z Klasy GRAJ oraz porównywanie otrzymywanych wyników w celu wybrania wymaganej akcji.


#endif