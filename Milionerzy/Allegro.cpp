#include "Allegro.h"
void Allegro::Start() {
	Pobierz_Odpowiedzi();
	al_init();
	al_init_image_addon();
	al_init_font_addon();
	al_init_ttf_addon();
	al_set_new_display_flags(ALLEGRO_FULLSCREEN_WINDOW);
	display = al_create_display(1000, 800);
	queue = al_create_event_queue();
	backgrund = al_load_bitmap("Milionerzy.png");
	font = al_load_ttf_font("ZakirahsHand.ttf", 50, 0);
	telefon = al_load_bitmap("Telefon.png");
	pnp = al_load_bitmap("50na50.png");
	publicznosc = al_load_bitmap("Publicznosc.png");
	dobrze = al_load_bitmap("dobrze.png");
	brawo = al_load_bitmap("brawo.png");
	running = true;
	info = true;
	play = true;
	display_height = al_get_display_height(display);
	display_width = al_get_display_width(display);
	backgrund_height = al_get_bitmap_height(backgrund);
	backgrund_width = al_get_bitmap_width(backgrund);
	brawo_width = al_get_bitmap_width(brawo);
	brawo_height = al_get_bitmap_height(brawo);
	kolo_height = al_get_bitmap_height(pnp);
	kolo_width = al_get_bitmap_height(pnp);
	font_height = al_get_font_line_height(font);
	font_GRAJ_width = al_get_text_width(font, "GRAJ");
	font_OK_width = al_get_text_width(font, "OK");
	font_INSTRUKCJA_width = al_get_text_width(font, "INSTRUKCJA");
	font_WYJDZ_width = al_get_text_width(font, "WYJDZ");
	font_MENU_width = al_get_text_width(font, "MENU");
	font_OdpA_width = al_get_text_width(font, odpowiedzi[0].c_str());
	font_OdpB_width = al_get_text_width(font, odpowiedzi[1].c_str());
	font_OdpC_width = al_get_text_width(font, odpowiedzi[2].c_str());
	font_OdpD_width = al_get_text_width(font, odpowiedzi[3].c_str());

	al_install_mouse();
	al_register_event_source(queue, al_get_mouse_event_source());
	al_register_event_source(queue, al_get_display_event_source(display));

	MainFunc();
}
void Allegro::MainFunc() {
	while (running)
	{
		al_draw_bitmap(backgrund, display_width / 2 - backgrund_width / 2, display_height / 2 - backgrund_height / 2, NULL);
		al_draw_text(font, al_map_rgb(191, 175, 0), display_width / 2, display_height * 22 / 100, 1, "GRAJ");
		al_draw_text(font, al_map_rgb(150, 255, 150), display_width / 2, display_height * 35 / 100, 1, "INSTRUKCJA");
		al_draw_text(font, al_map_rgb(194, 0, 0), display_width / 2, display_height * 65 / 100, 1, "WYJDZ");


		ALLEGRO_EVENT event;
		al_wait_for_event(queue, &event);

		if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			Click();
		}
		al_flip_display();
	}
	al_uninstall_mouse();
	al_destroy_display(display);
	al_destroy_bitmap(backgrund);
	al_destroy_font(font);

}
void Allegro::Lost()
{
	lose = true;
	while (lose)
	{
		al_draw_bitmap(backgrund, display_width / 2 - backgrund_width / 2, display_height / 2 - backgrund_height / 2, NULL);
		al_draw_text(font, al_map_rgb(0, 150, 50), display_width / 2, display_height * 30 / 100, 1, "PRZEGRALES NA PYTANIU O");
		al_draw_text(font, al_map_rgb(0, 150, 50), display_width / 2, display_height * 40 / 100, 1, kasa[numer_pytania - 1].c_str());
		al_draw_text(font, al_map_rgb(82, 178, 15), display_width / 2, display_height * 50 / 100, 1, "MENU");
		al_draw_text(font, al_map_rgb(194, 0, 0), display_width / 2, display_height * 60 / 100, 1, "WYJDZ");

		ALLEGRO_EVENT event;
		al_wait_for_event(queue, &event);
		if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			int x, y;
			al_get_mouse_cursor_position(&x, &y);
			if (display_width / 2 - font_WYJDZ_width / 2 < x && x < display_width / 2 + font_WYJDZ_width / 2 && display_height * 60 / 100 < y && y < display_height * 60 / 100 + font_height)
			{
				lose = false;
				running = false;
			}
			if (display_width / 2 - font_MENU_width / 2 < x && x < display_width / 2 + font_MENU_width / 2 && display_height * 50 / 100 < y && y < display_height * 50 / 100 + font_height)
			{
				numer_pytania = 0;
				Pobierz_Odpowiedzi();
				kolo1 = true;
				kolo2 = true;
				kolo3 = true;
				lose = false;
			}

		}
		al_flip_display();
	}
}
void Allegro::Good()
{
	good = true;
	while (good)
	{

		al_draw_bitmap(dobrze, display_width / 2 - brawo_width / 2, display_height / 2 - brawo_height / 2, NULL);
		al_draw_text(font, al_map_rgb(191, 175, 0), display_width / 2, display_height * 30 / 100, 1, "POPRAWNA ODPOWIEDZ");
		al_draw_text(font, al_map_rgb(191, 175, 0), display_width / 2, display_height * 40 / 100, 1, "KOLEJNE PYTANIE O");
		al_draw_text(font, al_map_rgb(191, 175, 0), display_width / 2, display_height * 50 / 100, 1, kasa[numer_pytania].c_str());
		al_draw_text(font, al_map_rgb(0, 0, 0), display_width / 2, display_height * 70 / 100, 1, "OK");

		ALLEGRO_EVENT event;
		al_wait_for_event(queue, &event);
		if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			int x, y;
			al_get_mouse_cursor_position(&x, &y);

			if (display_width / 2 - font_OK_width / 2 < x && x < display_width / 2 + font_OK_width / 2 && display_height * 70 / 100 < y && y < display_height * 70 / 100 + font_height)
			{
				good = false;
			}

		}
		al_flip_display();
	}
}
void Allegro::Win()
{
	win = true;
	while (win)
	{
		al_draw_bitmap(backgrund, display_width / 2 - backgrund_width / 2, display_height / 2 - backgrund_height / 2, NULL);
		al_draw_bitmap(brawo, display_width / 2 - brawo_width / 2, display_height / 2 - brawo_height / 2, NULL);
		al_draw_text(font, al_map_rgb(191, 175, 0), display_width / 2, display_height * 30 / 100, 1, "BRAWO, WYGRALES");
		al_draw_text(font, al_map_rgb(191, 175, 0), display_width / 2, display_height * 40 / 100, 1, kasa[numer_pytania - 1].c_str());
		al_draw_text(font, al_map_rgb(82, 178, 15), display_width / 2, display_height * 50 / 100, 1, "MENU");
		al_draw_text(font, al_map_rgb(194, 0, 0), display_width / 2, display_height * 60 / 100, 1, "WYJDZ");

		ALLEGRO_EVENT event;
		al_wait_for_event(queue, &event);
		if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			int x, y;
			al_get_mouse_cursor_position(&x, &y);
			if (display_width / 2 - font_WYJDZ_width / 2 < x && x < display_width / 2 + font_WYJDZ_width / 2 && display_height * 60 / 100 < y && y < display_height * 60 / 100 + font_height)
			{
				win = false;
				running = false;
			}
			if (display_width / 2 - font_MENU_width / 2 < x && x < display_width / 2 + font_MENU_width / 2 && display_height * 50 / 100 < y && y < display_height * 50 / 100 + font_height)
			{
				win = false;
			}

		}
		al_flip_display();
	}
}
void Allegro::Info()
{
	info = true;
	while (info)
	{
		al_draw_bitmap(backgrund, display_width / 2 - backgrund_width / 2, display_height / 2 - backgrund_height / 2, NULL);
		al_draw_text(font, al_map_rgb(150, 255, 150), display_width / 2, display_height * 5 / 100, 1, "Cel gry - odpowiedz poprawnie na 12 pytan, wygraj 1 000 000 zl!");
		al_draw_text(font, al_map_rgb(150, 255, 150), display_width / 2, display_height * 15 / 100, 1, "Koniec gry - bledna odpowiedz gracza.");
		al_draw_text(font, al_map_rgb(150, 255, 150), display_width / 2, display_height * 25 / 100, 1, "Jednorazowe kola ratunkowe:");
		al_draw_text(font, al_map_rgb(150, 255, 150), display_width / 2, display_height * 35 / 100, 1, "- 50/50 (zaznacza na czerwono dwie bledne odpowiedzi)");
		al_draw_text(font, al_map_rgb(150, 255, 150), display_width / 2, display_height * 45 / 100, 1, "- telefon (80% na zaznaczenie poprawnej odpowiedzi na zielono)");
		al_draw_text(font, al_map_rgb(150, 255, 150), display_width / 2, display_height * 55 / 100, 1, "- publicznosc (pokazuje procent publicznosci glosujacej na kazda z odpowiedzi).");

		al_draw_text(font, al_map_rgb(82, 178, 15), display_width / 2, display_height * 70 / 100, 1, "MENU");
		ALLEGRO_EVENT event;
		al_wait_for_event(queue, &event);
		if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			int x, y;
			al_get_mouse_cursor_position(&x, &y);
			if (display_width / 2 - font_MENU_width / 2 < x && x < display_width / 2 + font_MENU_width / 2 && display_height * 70 / 100 < y && y < display_height * 70 / 100 + font_height)
			{

				info = false;


			}
		}


		al_flip_display();
	}
}
void Allegro::Play()
{
	play = true;
	while (play)
	{
		al_draw_bitmap(backgrund, display_width / 2 - backgrund_width / 2, display_height / 2 - backgrund_height / 2, NULL);
		al_draw_text(font, al_map_rgb(100, 1000, 0), display_width / 2, display_height / 20, 1, tresc_pytania.c_str());
		al_draw_text(font, al_map_rgb(200, 200, 200), display_width * 25 / 100, display_height * 20 / 100, 1, odpowiedzi[0].c_str());
		al_draw_text(font, al_map_rgb(200, 200, 200), display_width * 75 / 100, display_height * 20 / 100, 1, odpowiedzi[1].c_str());
		al_draw_text(font, al_map_rgb(200, 200, 200), display_width * 25 / 100, display_height * 50 / 100, 1, odpowiedzi[2].c_str());
		al_draw_text(font, al_map_rgb(200, 200, 200), display_width * 75 / 100, display_height * 50 / 100, 1, odpowiedzi[3].c_str());
		al_draw_text(font, al_map_rgb(0, 0, 0), display_width * 65 / 100, display_height * 90 / 100, 1, "Pytanie o");
		al_draw_text(font, al_map_rgb(191, 175, 0), display_width * 80 / 100, display_height * 90 / 100, 1, kasa[numer_pytania - 1].c_str());
		al_draw_bitmap(pnp, display_width / 20, display_height * 8 / 10, NULL);
		al_draw_bitmap(publicznosc, display_width / 20 + kolo_width * 3 / 2, display_height * 8 / 10, NULL);
		al_draw_bitmap(telefon, display_width / 20 + kolo_width * 3, display_height * 8 / 10, NULL);

		ALLEGRO_EVENT event;
		al_wait_for_event(queue, &event);
		if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			int x, y;
			al_get_mouse_cursor_position(&x, &y);
			if (display_width * 25 / 100 - font_OdpA_width / 2 < x && x < display_width * 25 / 100 + font_OdpA_width / 2 && display_height * 20 / 100 < y && y < display_height * 20 / 100 + font_height)
			{
				wybrana_odpowiedz = "a";
				play = false;

			}
			else if (display_width * 75 / 100 - font_OdpB_width / 2 < x && x < display_width * 75 / 100 + font_OdpB_width / 2 && display_height * 20 / 100 < y && y < display_height * 20 / 100 + font_height)
			{
				wybrana_odpowiedz = "b";
				play = false;

			}
			else if (display_width * 25 / 100 - font_OdpC_width / 2 < x && x < display_width * 25 / 100 + font_OdpC_width / 2 && display_height * 50 / 100 < y && y < display_height * 50 / 100 + font_height)
			{
				wybrana_odpowiedz = "c";
				play = false;
			}
			else if (display_width * 75 / 100 - font_OdpD_width / 2 < x && x < display_width * 75 / 100 + font_OdpD_width / 2 && display_height * 50 / 100 < y && y < display_height * 50 / 100 + font_height)
			{
				wybrana_odpowiedz = "d";
				play = false;
			}
			else if (display_width / 20 + kolo_width * 3 < x && x < display_width / 20 + kolo_width * 4 && display_height * 90 / 100 - kolo_height / 2 < y && y < display_height * 90 / 100 + kolo_height / 2)
			{
				if (kolo1)
				{
					Telefon();
					Odpowiedz_Przyjaciela();
					kolo1 = false;
				}



			}
			else if (display_width / 20 + kolo_width * 3 / 2 < x && x < display_width / 20 + kolo_width * 5 / 2 && display_height * 90 / 100 - kolo_height / 2 < y && y < display_height * 90 / 100 + kolo_height / 2)
			{
				if (kolo2)
				{
					Publicznosc();
					Pokaz_Procenty();
					kolo2 = false;
				}



			}
			else if (display_width / 20 < x && x < display_width / 20 + kolo_width && display_height * 90 / 100 - kolo_height / 2 < y && y < display_height * 90 / 100 + kolo_height / 2)
			{
				if (kolo3)
				{
					PnP();
					Szukaj_Wylosowanych();
					kolo3 = false;
				}


			}
		}


		al_flip_display();

	}
}
void Allegro::Click()
{

	int x, y;
	al_get_mouse_cursor_position(&x, &y);
	if (display_width / 2 - font_WYJDZ_width / 2 < x && x < display_width / 2 + font_WYJDZ_width / 2 && display_height * 65 / 100 < y && y < display_height * 65 / 100 + font_height)
	{
		running = false;
	}
	else if (display_width / 2 - font_INSTRUKCJA_width / 2 < x && x < display_width / 2 + font_INSTRUKCJA_width / 2 && display_height * 35 / 100 < y && y < display_height * 35 / 100 + font_height)
	{
		Info();
	}
	else if (display_width / 2 - font_GRAJ_width / 2 < x && x < display_width / 2 + font_GRAJ_width / 2 && display_height * 22 / 100 < y && y < display_height * 22 / 100 + font_height)
	{
		Play();
		while (wybrana_odpowiedz == poprawna_odpowiedz)
		{
			if (numer_pytania == 12)
			{
				Win();
				wybrana_odpowiedz = "";
				numer_pytania = 0;
				Pobierz_Odpowiedzi();
			}
			else
			{
				Good();
				Pobierz_Odpowiedzi();
				Play();
			}
		}
		if (wybrana_odpowiedz != "")
			Lost();

	}

}
void Allegro::Szukaj_Wylosowanych()
{
	if (wylosowana == odpowiedzi[0] || wylosowana1 == odpowiedzi[0])
		al_draw_text(font, al_map_rgb(133, 2, 2), display_width * 25 / 100, display_height * 20 / 100, 1, odpowiedzi[0].c_str());
	if (wylosowana == odpowiedzi[1] || wylosowana1 == odpowiedzi[1])
		al_draw_text(font, al_map_rgb(133, 2, 2), display_width * 75 / 100, display_height * 20 / 100, 1, odpowiedzi[1].c_str());
	if (wylosowana == odpowiedzi[2] || wylosowana1 == odpowiedzi[2])
		al_draw_text(font, al_map_rgb(133, 2, 2), display_width * 25 / 100, display_height * 50 / 100, 1, odpowiedzi[2].c_str());
	if (wylosowana == odpowiedzi[3] || wylosowana1 == odpowiedzi[3])
		al_draw_text(font, al_map_rgb(133, 2, 2), display_width * 75 / 100, display_height * 50 / 100, 1, odpowiedzi[3].c_str());
}
void Allegro::Odpowiedz_Przyjaciela()
{
	if (wybor_przyjaciela == odpowiedzi[0])
		al_draw_text(font, al_map_rgb(48, 194, 25), display_width * 25 / 100, display_height * 20 / 100, 1, odpowiedzi[0].c_str());
	if (wybor_przyjaciela == odpowiedzi[1])
		al_draw_text(font, al_map_rgb(48, 194, 25), display_width * 75 / 100, display_height * 20 / 100, 1, odpowiedzi[1].c_str());
	if (wybor_przyjaciela == odpowiedzi[2])
		al_draw_text(font, al_map_rgb(48, 194, 25), display_width * 25 / 100, display_height * 50 / 100, 1, odpowiedzi[2].c_str());
	if (wybor_przyjaciela == odpowiedzi[3])
		al_draw_text(font, al_map_rgb(48, 194, 25), display_width * 75 / 100, display_height * 50 / 100, 1, odpowiedzi[3].c_str());

}
void Allegro::Pokaz_Procenty()
{
	if (poprawna_odpowiedz2 == odpowiedzi[0])
	{
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 5 / 100 , display_height * 20 / 100, 1, szansa_poprawna.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 9 / 100 , display_height * 20 / 100, 1, "%");
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 92 / 100 , display_height * 20 / 100, 1, szansa_bledna1.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 96 / 100 , display_height * 20 / 100, 1, "%");
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 5 / 100 , display_height * 50 / 100, 1, szansa_bledna2.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 9 / 100 , display_height * 50 / 100, 1, "%");
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 92 / 100 , display_height * 50 / 100, 1, szansa_bledna3.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 96 / 100 , display_height * 50 / 100, 1, "%");
	}
	if (poprawna_odpowiedz2 == odpowiedzi[1])
	{
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 5 / 100, display_height * 20 / 100, 1, szansa_bledna1.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 9 / 100, display_height * 20 / 100, 1, "%");
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 92 / 100, display_height * 20 / 100, 1, szansa_poprawna.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 96 / 100, display_height * 20 / 100, 1, "%");
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 5 / 100 , display_height * 50 / 100, 1, szansa_bledna2.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 9 / 100 , display_height * 50 / 100, 1, "%");
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 92 / 100, display_height * 50 / 100, 1, szansa_bledna3.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 96 / 100, display_height * 50 / 100, 1, "%");
	}
	if (poprawna_odpowiedz2 == odpowiedzi[2])
	{
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 5 / 100 , display_height * 20 / 100, 1, szansa_bledna1.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 9 / 100 , display_height * 20 / 100, 1, "%");
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 92 / 100 , display_height * 20 / 100, 1, szansa_bledna2.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 96 / 100 , display_height * 20 / 100, 1, "%");
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 5 / 100 , display_height * 50 / 100, 1, szansa_poprawna.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 9 / 100 , display_height * 50 / 100, 1, "%");
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 92 / 100 , display_height * 50 / 100, 1, szansa_bledna3.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 96 / 100 , display_height * 50 / 100, 1, "%");
	}
	if (poprawna_odpowiedz2 == odpowiedzi[3])
	{
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 5 / 100 , display_height * 20 / 100, 1, szansa_bledna1.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 9 / 100 , display_height * 20 / 100, 1, "%");
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 92 / 100 , display_height * 20 / 100, 1, szansa_bledna2.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 96 / 100 , display_height * 20 / 100, 1, "%");
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 5 / 100 , display_height * 50 / 100, 1, szansa_bledna3.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 9 / 100 , display_height * 50 / 100, 1, "%");
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 92 / 100 , display_height * 50 / 100, 1, szansa_poprawna.c_str());
		al_draw_text(font, al_map_rgb(255, 0, 255), display_width * 96 / 100 , display_height * 50 / 100, 1, "%");
	}
}
void Graj::Pobierz_Odpowiedzi()
{

	fstream plik;
	string linia;
	plik.open("Panel.txt", ios::in);
	if (plik.good() == true)
	{
		cout << numer_pytania << endl;
		for (int i = 1; i <= numer_pytania * 6; i++)
		{
			getline(plik, linia);
			string zabijacz = linia;
		}
		getline(plik, linia);
		tresc_pytania = linia;
		getline(plik, linia);
		poprawna_odpowiedz = linia;
		for (int i = 0; i < ilosc_odpowiedzi; i++)
		{
			getline(plik, linia);
			odpowiedzi[i] = linia;


		}
		plik.close();
		numer_pytania++;
	}

}
void Graj::TablicaNiepoprawne()
{
	int x = 0;
	odpowiedzi2[0] = "a";
	odpowiedzi2[1] = "b";
	odpowiedzi2[2] = "c";
	odpowiedzi2[3] = "d";
	for (int i = 0; i < 4; i++) {
		if (odpowiedzi2[i] != poprawna_odpowiedz)
		{
			niepoprawne[x] = odpowiedzi[i];
			x++;
		}
		else if (odpowiedzi2[i] == poprawna_odpowiedz)
			poprawna_odpowiedz2 = odpowiedzi[i];

	}

}
void Graj::PnP()
{
	int a;
	int b;
	TablicaNiepoprawne();
	srand(time(NULL));
	a = (rand() % 3);
	b = (rand() % 3);
	while (b == a)
	{
		b = (rand() % 3);
	}
	wylosowana = niepoprawne[a];
	wylosowana1 = niepoprawne[b];

}
void Graj::Telefon()
{
	TablicaNiepoprawne();
	for (int i = 0; i < 8; i++) {
		tab[i] = poprawna_odpowiedz2;
	}
	int a;
	srand(time(NULL));
	a = (rand() % 3);
	tab[8] = niepoprawne[a];
	tab[9] = niepoprawne[a];
	int b;
	srand(time(NULL));
	b = (rand() % 9);
	wybor_przyjaciela = tab[b];

}
void Graj::Publicznosc()
{
	int a = 0;
	int b = 0;
	int c = 0;
	int d = 0;
	int x;
	TablicaNiepoprawne();
	srand(time(NULL));
	for (int i = 0; i < 5; i++) {
		publicznosc[i] = poprawna_odpowiedz2;
	}
	publicznosc[5] = niepoprawne[0];
	publicznosc[6] = niepoprawne[0];
	publicznosc[7] = niepoprawne[1];
	publicznosc[8] = niepoprawne[1];
	publicznosc[9] = niepoprawne[2];
	for (int i = 0; i < 10; i++)
	{
		x = (rand() % 9);
		if (x == 0 || x == 2 || x == 3 || x == 4 || x == 1)
		{
			a++;
		}
		if (x == 5 || x == 6)
		{
			b++;
		}
		if (x == 7 || x == 8)
		{
			c++;
		}
		if (x == 9)
		{
			d++;
		}

	}
	szansa_poprawna = to_string(a * 10);
	szansa_bledna1 = to_string(b * 10);
	szansa_bledna2 = to_string(c * 10);
	szansa_bledna3 = to_string(d * 10);
}
